﻿(function ($) {
    $(document).ready(function () {
        $(document).on("click", ".Consult", function () {
            var Current = this;

            if ($(Current).parent().find('.Name').val().length == 0 ||
                $(Current).parent().find('.Phone').val().length == 0) {
                $(Current).parent().find('.Success, .Error').remove();
                $(Current).parent().append("<p class='Error'>Повторите попытку</p>");
            } else {
                $.ajax({
                    url: 'http://bunker-a.com/scripts/lavka_mail.php',
                    type: 'POST',
                    data: $(Current).parent().serialize(),
                    success: function () {

                        $(Current).parent().find('.Success, .Error').remove();
                        $(Current).parent().append("<p class='Success'>Заявка принята!</p>");
                        $(Current).parent().find('.Name').val('');
                        $(Current).parent().find('.Phone').val('');

                    },
                    error: function () {
                        $(Current).parent().find('.Success, .Error').remove();

                    }

                });
            }
        });

    });
})(jQuery);
