<!DOCTYPE HTML>
<html>
<head>
    <jdoc:include type="head"/>
    <meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
    <link rel="icon" type="image/png" href="" />

	
    <script src="/templates/protostar/js/js.js"></script>
    <link rel="stylesheet" href="/templates/protostar/css/style.css"/>
    <link rel="stylesheet" href="/templates/protostar/css/media.css"/>
    <link href='http://fonts.googleapis.com/css?family=Open+Sans:400,700&subset=cyrillic' rel='stylesheet' type='text/css'>
    <link rel="stylesheet" href="//maxcdn.bootstrapcdn.com/font-awesome/4.3.0/css/font-awesome.min.css">

    <script>
    function printDiv(printableArea) {
		var printContents = document.getElementById(printableArea).innerHTML;
		var originalContents = document.body.innerHTML;

		document.body.innerHTML = printContents;
		window.print();
		document.body.innerHTML = originalContents;
    }
    </script>
</head>
<body>
    <div class="Wrapper-All">
        <div class="Wrapper">
            <div class="Head">
                <div class="Left-Col-Head">
                    <a data-lightbox="width:480 !important;height:360 !important;" href="/index.php?option=com_content&view=article&id=2&tmpl=component"><img src="/images/logo-head.png" alt=""/></a>
                    <p class="Left-Col-Text-1">ООО "Дебет-Медиация"</p>
                    <p class="Left-Col-Text-2">Честная работа в условиях доверия</p>
                </div>
                <div class="Right-Col-Head">
                    <p class="Right-Col-Text">Отдел досудебного урегулирования споров:</p>
                    <p class="Right-Col-Text">127006, Москва, ул. Петровка, д. 32, стр. 2</p>
                    <p class="Right-Col-Text">Юридический отдел:</p>
                    <p class="Right-Col-Text">125009, Москва, Георгиевский пер., д.1, стр.2</p>
                    <span class="Right-Col-Text s1">Тел:</span><span>+7 (495) 505 30 13</span><br>
                    <span class="s2">E-mail:</span><span class="s3"><a href="mailto:info@debtmediation.ru">info@debtmediation.ru</a></span>
                </div>
            </div>


            <div class="Sub-Head">
                <p>Взыскание долгов</p>
                <p>с физических и юридических</p>
                <p>лиц</p>
            </div>


            <div class="Sub-Head-2">
                <p class="Sub-Head-2-Text-1">Специальное предложение!!!</p>
                <p class="Sub-Head-2-Text-1">Арбитраж «под ключ» за 55 000 рублей.</p>
                <a data-lightbox="on" href='/index.php?option=com_content&view=article&id=10&tmpl=component' class="Vospolzocatsa">Воспользоваться предложением!</a>
                <p class="Sub-Head-2-Text-2">Мы профессиональные юристы, адвокаты,</p>
                <p class="Sub-Head-2-Text-2">переговорщики и медиаторы</p>
            </div>


            <div class="Why-We">
                <div class="Why-We-Left">
                    <p class="Why-We-Left-Head">Почему стоит</p>
                    <p class="Why-We-Left-Head">обратиться к нам?</p>
                    <div class="Why-We-Left-Blocks">
                        <div class="Why-We-Left-Block">
                            <div class="Why-We-Left-Block-1">
                                <img src="/images/logo-head.png" alt=""/>
                            </div>
                            <div class="Why-We-Left-Block-2">
                                <span>Мы профессионалы</span>
                                <p>Наши сотрудники имеют опыт работы в органах <br>правопорядка</p>
                            </div>
                        </div>
                        <div class="Why-We-Left-Block">
                            <div class="Why-We-Left-Block-1">
                                <img src="/images/logo-head.png" alt=""/>
                            </div>
                            <div class="Why-We-Left-Block-2">
                                <span>Мы эффективны</span>
                                <p>Наша команда работает с 2011 года –
                                    за это время фактически взыскали более
                                    800 000 000 рублей для наших клиентов.
                                    Мы не проигрываем в Арбитраже!!!</p>
                                <p>У нас 100% выигранных дел.</p>
                            </div>
                        </div>
                        <div class="Why-We-Left-Block">
                            <div class="Why-We-Left-Block-1">
                                <img src="/images/logo-head.png" alt=""/>
                            </div>
                            <div class="Why-We-Left-Block-2">
                                <span>Мы действительно любим свою работу</span>
                                <p>Наша команда состоит из высококвалифицированных специалистов,
                                    каждый из которых имеет многолетний опыт работы во
                                    взысканиях на всех его этапах. Мы представляем интересы
                                    взыскателей уже много лет и знаем все секреты успешного
                                    взыскания проблемной задолженности.</p>
                            </div>
                        </div>
                        <div class="Why-We-Left-Block">
                            <div class="Why-We-Left-Block-1">
                                <img src="/images/logo-head.png" alt=""/>
                            </div>
                            <div class="Why-We-Left-Block-2">
                                <span>Мы всегда на связи</span>
                                <p>Вы регулярно получаете отчет о проведенной работе
                                    в согласованное с нами время, а также в любой момент
                                    можете позвонить и поинтересоваться о ходе движения Вашего дела</p>
                                <p>Для наших заказчиков мы на связи 24 часа в сутки!</p>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="Why-We-Right">
                    <p class="Why-We-Right-Head">Получить бесплатную консультацию</p>
                    <div class="Why-We-Right-Block">
                        <div class="Why-We-Right-Block-1">
                            <p>Заявка</p>
                        </div>
                        <form class="Why-We-Right-Block-2">
                            <input class="Name" type="text" placeholder="Имя *" name="name"/>
                            <input class="Phone" type="text" placeholder="Телефон *" name="phone"/>
                            <input class="Consult" type="button" value="Бесплатная консультация!" name="consult"/>
                        </form>
                    </div>
                </div>
            </div>


            <div class="Not-Execution">
                <p class="Not-Execution-Head">ВАШИ ПАРТНЕРЫ НЕ ИСПОЛНЯЮТ ДОГОВОРНЫХ</p>
                <p class="Not-Execution-Head">ОБЯЗАТЕЛЬСТВ???</p>
                <span class="Call">ЗВОНИТЕ НАМ!!!</span><span class="Call-Phone">+7 495 505 30 13</span>
                <p class="Not-Execution-Head-2">ИЛИ ОСТАВЬТЕ ЗАЯВКУ, МЫ ПЕРЕЗВОНИМ ВАМ:</p>
                <div class="Why-We-Right-Block">
                    <div class="Why-We-Right-Block-1">
                        <p>Заявка</p>
                    </div>
                    <form class="Why-We-Right-Block-2">
                        <input class="Name" type="text" placeholder="Имя *" name="name"/>
                        <input class="Phone" type="text" placeholder="Телефон *" name="phone"/>
                        <input class="Consult" type="button" value="Отправить заявку!" name="consult"/>
                    </form>
                </div>
            </div>


            <div class="Special-Offer">
                <p class="Special-Offer-Head-1">СПЕЦИАЛЬНОЕ ПРЕДЛОЖЕНИЕ</p>
                <p class="Special-Offer-Head-2">АРБИТРАЖ «ПОД КЛЮЧ» - 55 000 рублей</p>
                <div class="Special-Offer-Text">
                    <p>Предложение действительно при наличии всех оригиналов документов и доказательств, необходимых для процесса.
                        Распространяется на первую инстанцию в арбитражном суде или суде общей юрисдикции.</p>
                    <p>Мы гарантируем Вам получение исполнительного листа.</p>
                    <p>Это предложение для Вас!!! Звоните + 7 495 505 30 13</p>
                    <a data-lightbox="on" href='/index.php?option=com_content&view=article&id=10&tmpl=component' class="Vospolzocatsa">Воспользоваться предложением!</a>
                </div>
            </div>


            <div class="Services">
                <p class="Services-Head">УСЛУГИ</p>
                <div class="Services-Blocks">
                    <a data-lightbox="group:serv;width:600" href='/index.php?option=com_content&view=article&id=1&tmpl=component' class="Services-Block">
                        <img src="/images/services-1.jpg" alt=""/>
                        <p>Представление интересов</p>
                        <p>взыскателя в арбитражном суде и</p>
                        <p>суде общей юрисдикции</p>
                    </a>
                    <a data-lightbox="group:serv;width:600" href='/index.php?option=com_content&view=article&id=4&tmpl=component' class="Services-Block">
                        <img src="/images/services-2.jpg" alt=""/>
                        <p>Сопровождение исполнительного</p>
                        <p>производства</p>
                    </a>
                    <a data-lightbox="group:serv;width:600" href='/index.php?option=com_content&view=article&id=5&tmpl=component' class="Services-Block">
                        <img src="/images/services-3.jpg" alt=""/>
                        <p>Проверка контрагента, партнера</p>
                    </a>
                    <a data-lightbox="group:serv;width:600" href='/index.php?option=com_content&view=article&id=6&tmpl=component' class="Services-Block">
                        <img src="/images/services-4.jpg" alt=""/>
                        <p>Досудебное урегулирование</p>
                        <p>задолженности</p>
                    </a>
                    <a data-lightbox="group:serv;width:600" href='/index.php?option=com_content&view=article&id=7&tmpl=component' class="Services-Block">
                        <img src="/images/services-5.jpg" alt=""/>
                        <p>Юридическое сопровождение</p>
                        <p>бизнеса</p>
                    </a>
                    <a data-lightbox="group:serv;width:600" href='/index.php?option=com_content&view=article&id=8&tmpl=component' class="Services-Block">
                        <img src="/images/services-6.jpg" alt=""/>
                        <p>Консультации</p>
                    </a>
                </div>
            </div>


            <div class="Our-Partners">
                <p class="Our-Partners-Head">НАШИ ПАРТНЁРЫ</p>
                <img src="/images/partners-1.jpg" alt=""/>
                <img src="/images/partners-2.jpg" alt=""/>
                <img src="/images/partners-3.jpg" alt=""/>
                <img src="/images/partners-4.jpg" alt=""/>
                <img src="/images/partners-5.jpg" alt=""/>
                <img src="/images/partners-6.jpg" alt=""/>
                <img src="/images/partners-7.jpg" alt=""/>
                <img src="/images/partners-8.jpg" alt=""/>
                <img src="/images/partners-9.jpg" alt=""/>
                <img src="/images/partners-10.jpg" alt=""/>
                <img src="/images/partners-11.jpg" alt=""/>
                <img src="/images/partners-12.jpg" alt=""/>
                <img src="/images/partners-13.jpg" alt=""/>
                <img src="/images/partners-14.jpg" alt=""/>
                <img src="/images/partners-15.jpg" alt=""/>
                <img src="/images/partners-16.jpg" alt=""/>
            </div>


            <div class="Our-Partners">
                <p class="Our-Partners-Head">НАШИ ИНФОРМАЦИОННЫЕ ПАРТНЁРЫ</p>
                <img src="/images/info-partners-1.jpg" alt=""/>
                <img src="/images/info-partners-2.jpg" alt=""/>
                <img src="/images/info-partners-3.jpg" alt=""/>
                <img src="/images/info-partners-4.jpg" alt=""/>
                <img src="/images/info-partners-5.jpg" alt=""/>
                <img src="/images/info-partners-6.jpg" alt=""/>
                <img src="/images/info-partners-7.gif" alt=""/>
            </div>


            <div class="Our-Partners">
                <p class="Our-Partners-Head">НАМ ДОВЕРЯЮТ</p>
                <img src="/images/faith-1.jpg" alt=""/>
                <img src="/images/faith-2.jpg" alt=""/>
                <img src="/images/faith-3.jpg" alt=""/>
                <img src="/images/faith-4.jpg" alt=""/>
                <img src="/images/faith-5.jpg" alt=""/>
                <img src="/images/faith-6.jpg" alt=""/>
                <img src="/images/faith-7.jpg" alt=""/>
                <img src="/images/faith-8.jpg" alt=""/>
                <img src="/images/faith-9.jpg" alt=""/>
                <img src="/images/faith-10.jpg" alt=""/>
                <img src="/images/faith-11.jpg" alt=""/>
                <img src="/images/faith-12.jpg" alt=""/>
                <img src="/images/faith-13.jpg" alt=""/>
                <img src="/images/faith-14.jpg" alt=""/>
                <img src="/images/faith-15.jpg" alt=""/>
                <img src="/images/faith-16.jpg" alt=""/>
                <img src="/images/faith-17.jpg" alt=""/>
            </div>
        
        
        
            <div class="Recall">
                <p class="Recall-Head-1">ОТЗЫВЫ</p>
                <p class="Recall-Head-2">Мы гордимся тем фактом, что многие наши клиенты
                    узнают о нас по рекомендациям своих партнёров, знакомых и друзей</p>
                <div class="Recall-Block">
                    <p>ООО «ИНВЕСТ-КС»</p>
                    <i onclick="printDiv('printableArea_a')" class="fa fa-print"></i>
                    <a id="printableArea_a" data-lightbox="group:mygroup" href="/index.php?option=com_content&view=article&id=11&tmpl=component">
                        <img src="/images/recall-1.jpg" alt=""/>
                    </a>
                </div>
				
                <div class="Recall-Block">
                    <p> ООО «ВАВИН РУС» (КОНЦЕРН WAVIN)</p>
                    <i onclick="printDiv('printableArea_b')" class="fa fa-print"></i>
                    <a id="printableArea_b" data-lightbox="group:mygroup" href="/index.php?option=com_content&view=article&id=12&tmpl=component">
                        <img src="/images/recall-2.jpg" alt=""/>
                    </a>
                </div>
                <div class="Recall-Block">
                    <p>ОАО «КРАСНОСЕЛЬСКИЙ ЮВЕЛИРПРОМ» (АЛМАЗ-ХОЛДИНГ)</p>
                    <i onclick="printDiv('printableArea_c')" class="fa fa-print"></i>
                    <a id="printableArea_c" data-lightbox="group:mygroup" href="/index.php?option=com_content&view=article&id=13&tmpl=component">
                        <img src="/images/recall-3.jpg" alt=""/>
                    </a>
                </div>
                <div class="Recall-Block">
                    <p>ООО «ВВК ИНЖЕНИРИНГ»</p>
                    <i onclick="printDiv('printableArea_d')" class="fa fa-print"></i>
                    <a id="printableArea_d" data-lightbox="group:mygroup" href="/index.php?option=com_content&view=article&id=14&tmpl=component">
                        <img src="/images/recall-4.jpg" alt=""/>
                    </a>
                </div>
                <div class="Recall-Block">
                    <p>ООО «ОПТИМА»</p>
                    <i onclick="printDiv('printableArea_e')" class="fa fa-print"></i>
                    <a id="printableArea_e" data-lightbox="group:mygroup" href="/index.php?option=com_content&view=article&id=15&tmpl=component">
                        <img src="/images/recall-5.jpg" alt=""/>
                    </a>
                </div>
                <div class="Recall-Block">
                    <p>ООО «АВТО-СТАР ТРЕЙД»  (АВТОКОНТИНЕТ)</p>
                    <i onclick="printDiv('printableArea_f')" class="fa fa-print"></i>
                    <a id="printableArea_f" data-lightbox="group:mygroup" href="/index.php?option=com_content&view=article&id=16&tmpl=component">
                        <img src="/images/recall-6.jpg" alt=""/>
                    </a>
                </div>
                <div class="Recall-Block">
                    <p>ООО «ВЕСТ-ПЕРЕЕЗД»</p>
                    <i onclick="printDiv('printableArea_g')" class="fa fa-print"></i>
                    <a id="printableArea_g" data-lightbox="group:mygroup" href="/index.php?option=com_content&view=article&id=17&tmpl=component">
                        <img src="/images/recall-7.jpg" alt=""/>
                    </a>
                </div>
                <div class="Recall-Block">
                    <p>ООО «ФОТОН»</p>
                    <i onclick="printDiv('printableArea_h')" class="fa fa-print"></i>
                    <a id="printableArea_h" data-lightbox="group:mygroup" href="/index.php?option=com_content&view=article&id=18&tmpl=component">
                        <img src="/images/recall-8.jpg" alt=""/>
                    </a>
                </div>
                <div class="Recall-Block">
                    <p>ООО «ПОЛИМЕРТРАНС»</p>
                    <i onclick="printDiv('printableArea_i')" class="fa fa-print"></i>
                    <a id="printableArea_i" data-lightbox="group:mygroup" href="/index.php?option=com_content&view=article&id=19&tmpl=component">
                        <img src="/images/recall-9.jpg" alt=""/>
                    </a>
                </div>
                <div class="Recall-Block">
                    <p>ООО «ИРБИС-ВЕТ»</p>
                    <i onclick="printDiv('printableArea_j')" class="fa fa-print"></i>
                    <a id="printableArea_j" data-lightbox="group:mygroup" href="/index.php?option=com_content&view=article&id=20&tmpl=component">
                        <img src="/images/recall-10.jpg" alt=""/>
                    </a>
                </div>
                <div class="Recall-Block">
                    <p>ООО "Радуга Комфорта"</p>
                    <i onclick="printDiv('printableArea_k')" class="fa fa-print"></i>
                    <a id="printableArea_k" data-lightbox="group:mygroup" href="/index.php?option=com_content&view=article&id=21&tmpl=component">
                        <img src="/images/recall-11.jpg" alt=""/>
                    </a>
                </div>
                <div class="Recall-Block">
                    <p>ИП МОСОЛОВА</p>
                    <i onclick="printDiv('printableArea_l')" class="fa fa-print"></i>
                    <a id="printableArea_l" data-lightbox="group:mygroup" href="/images/recall-12.jpg">
                        <img src="/images/recall-12.jpg" alt=""/>
                    </a>
                </div>
            </div>


            <div class="Maps">
                <!--<img src="/images/vajno.jpg" alt=""/>-->
                <div class="Map-1">
                    <p class="Map-Head">Отдел досудебного урегулирования споров располагается по адресу:</p>
                    <p class="Map-Address">г.Москва, ул. Петровка, д.32, стр.2</p>
                    <div class="Map">
                        <script type="text/javascript" charset="utf-8" src="//api-maps.yandex.ru/services/constructor/1.0/js/?sid=69TcS4rm9lYc0Il4zhho_jWZxJOMj8TA&width=600&height=450&margin=600"></script>
                    </div>
                </div>

                <div class="Map-2">
                    <p class="Map-Head">Юридический отдел располагается по адресу:</p>
                    <p class="Map-Address">г.Москва, Георгиевский пер., д.1, стр.2</p>
                    <div class="Map">
                        <script type="text/javascript" charset="utf-8" src="//api-maps.yandex.ru/services/constructor/1.0/js/?sid=PELz_W1O38eR_K9zyFYIleRWbqseiIXh&width=600&height=450"></script>
                    </div>
                </div>
				<div class="Between-Maps">
                    <p class="Between-Maps-Text-1">ОСТАЛИСЬ ВОПРОСЫ??? ПОЗВОНИТЕ НАМ!!!</p>
                    <p class="Between-Maps-Text-2">+7 (495) 505 30 13</p>
                </div>
            </div>


        </div>
    </div>
</body>
</html>